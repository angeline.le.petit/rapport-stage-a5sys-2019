\chapter{Organisation}
\label{sec:organisation}

\section{Méthodologies}

\par Afin de bien s'organiser pour les développements, une méthode utilisant certains principes fondamentaux de la méthode Scrum (qui signifie mêlée en anglais) a été utilisée. Scrum est un cadre de travail itératif se concentrant sur les buts communs en livrant des produits ayant la plus grande valeur possible. La méthodologie de projet est structurée en un cycle de vie en 3 phases : 

\begin{enumerate}
    \item Une réunion de planification de sprint. Après cette réunion, l'équipe a décidé des fonctionnalités à développer et comment elle s'organisera pour la prochaine phase, le sprint ;
    \item Le sprint, qui est une période d'un mois au maximum, au bout de laquelle l'équipe délivre des fonctionnalités et donc une nouvelle version du logiciel, potentiellement livrable ;
    \item La revue de sprint. À la fin du sprint, l'équipe et les parties-prenantes (le client) invitées se réunissent pour passer en revue le sprint précédent. L'objectif est de valider la nouvelle version du produit qui a été réalisée. L'équipe effectue une démonstration des fonctionnalités développées. Les fonctionnalités non terminées ne sont pas présentées ;
\end{enumerate}

\par Dans notre cas, nous appliquons la majorité des éléments de Scrum, en particulier les réunions de planification de sprint qui sont appelées "réunions de lancement". Les fonctionnalités choisies pour chaque sprint sont assignées à un lot. Par exemple, le premier sprint avait pour but de livrer le lot N\textsuperscript{\underline{o}}1.

\par Certains éléments de la méthodologie du cycle en V sont aussi utilisés, comme la phase de recette ou encore la phase de conception.

\par À Chaque début de journée, une réunion de planification pour la journée entre les développeurs est mise en place. Tout autres personnes peuvent assister à cette réunion. Le principe de cette réunion est de, à tour de rôle, parler de trois choses :

\begin{itemize}
    \item ce qu'il a réalisé la veille ;
    \item ce qu'il compte réaliser aujourd'hui pour atteindre l'objectif du sprint actuel ;
    \item les obstacles et les problèmes qu'il rencontre.
\end{itemize}

\par Les fonctionnalités sont souvent découpées en petite sous-fonctionnalité pour mieux se rendre compte de la charge de travail et pouvoir ainsi chiffrer* (estimer la charge de travail). Ces petites fonctionnalités sont ensuite ajoutées a un tableau kanban* (un tableau de tâche fonctionnelles avec des colonnes représentant l'état de ces tâches, comme 'en cours de développement', ou encore 'à valider') permettant de : 

\begin{itemize}
    \item visualiser le flux de travail ;
    \item tracer l'état de chaque tâches ;
    \item affecter les tâches à des personnes ;
    \item surveiller, adapter, améliorer le travail en général.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=14cm]{images/kanban-trello.png}
    \caption{Exemple d'utilisation d'un tableau kanban (sur Trello)}
    \label{fig:kanbantrello}
\end{figure}

\par L'outil Trello est utilisé pour avoir un tableau kanban virtuel, trois colonnes sont utilisées, une colonne 'à faire' pour le travail restant, une colonne 'en cours' pour le travail en cours et une colonne 'terminé' pour le travail terminé. La case blanche représentent des tâches.

\par Cette méthode de projet a été, à mon sens, très adaptée au projet sur lequel nous avons travaillé, car la gestion des tâches restantes était très organisée grâce au tableau kanban. Nous avions aussi une très bonne vision du travail des autres avec les petites réunions chaque jour, ce qui permet de prévoir et de connaître l'état du projet.

\section{Processus global}

\par Nous avons aussi suivi un processus utilisé à A5Sys, basé sur le modèle en V, pour la livraison de logiciel de ce projet. Ce processus est divisé en trois grandes parties :

\begin{enumerate}
    \item Le processus de conception, où l'on recueille les besoins du client, on définit des règles de gestion ainsi que des maquettes, et on fini par valider la conception auprès client ;
    \item Le processus de développement, pendant lequel on développe les fonctionnalités conçues, une vérification est ensuite effectué par un référent technique, puis ces développements sont validés par le chef de projet ;
    \item Le processus de mise en production, où le client qualifie et valide les développements précédents pendant la phase de recette, puis, si ces développements sont valides, ils seront inclus dans la prochaine version du logiciel, autrement dit, mis en production.
\end{enumerate}

\newpage

\subsection{Processus de conception}

\begin{figure}[h]
    \centering
    \includegraphics[width=4cm]{images/schemas/processusconception.png}
    \caption{Processus de conception}
    \label{fig:processusconception}
\end{figure}

\par Le processus de conception (ref: \ref{fig:processusconception}) définis les étapes permettant de concevoir une fonctionnalité pour une application ou un projet au sein d'A5Sys. Il faut commencer par recueillir les besoins du client, pour ensuite les transformer en spécifications fonctionnelles (une sorte de cahier des charges). Ces spécifications doivent être lisibles par n'importe quelle personne et doivent être comprises par le client, donc le rédacteur doit mesurer ses mots.

\par Il faut savoir que les missions de conception peuvent être divisées en trois parties : 
\begin{itemize}
    \item Décrire en français la fonctionnalité ;
    \item Définir les règles de gestion ;
    \item Établir une maquette de la fonctionnalité si besoin.
\end{itemize}

\newpage

\subsection{Processus de développement}

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm]{images/schemas/processusdeveloppement.png}
    \caption{Processus de développement}
    \label{fig:processusdeveloppement}
\end{figure}
 
\par Le processus de développement (ref: \ref{fig:processusdeveloppement}) à A5Sys requiert les compétences de certaines personnes pour achever son cycle : 

\begin{itemize}
    \item Le développeur, celui qui développe la fonctionnalité ;
    \item Le développeur expérimenté, qui vérifie le travail du développeur ;
    \item Le chef de projet, qui peut qualifier le travail du développeur fonctionnellement ;
    \item Le client, qui estime si la fonctionnalité développée est correct par rapport à ses besoins.
\end{itemize}

\par En premier lieu, le développeur développe la fonctionnalité conformément aux spécifications et à la demande du client, puis, un autre développeur, de préférence plus expérimenté, effectue une relecture des développements. Si le développement est correct au niveau du code, la fonctionnalité est qualifiée par une personne compétente, souvent le chef de projet. Si la fonctionnalité est conforme, elle va être ajoutée à la version principale de l'application (appelé branche) sur la plateforme de gestion de version GitLab.

\subsection{Processus de mise en production}

\par Le plus souvent, les fonctionnalités sont développées par lot de plusieurs fonctionnalités et correctifs. Si toutes les fonctionnalités du lot sont développées, alors c'est l'étape de la mise en recette. Une recette (autrement appelé test d'acceptation) est une phase de développement d'un projet informatique qui vérifie que le produit est conforme à la spécification dans son ensemble. C'est le client qui essaye son propre produit avant qu'il ne soit sortie officiellement. L'application est mise en recette par le biais de Jenkins, qui est un outil d'intégration et de déploiement continu. Il permet de déployer une application sur un serveur pour qu'elle soit disponible pour le client et les personnes concernées.

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm]{images/schemas/processusmep.png}
    \caption{Processus de mise en production}
    \label{fig:processusmep}
\end{figure}

\par Pour le processus de mise en production, il faut tout d'abord préparer Jenkins pour pouvoir mettre sur un serveur l'application avec les bons paramètres et les bonnes données dans la base de données. Ensuite, le client peut commencer à tester l'application directement sur le serveur de la recette, pour faire un retour au chef de projet et aux développeurs de ce qui est correct ou non. Si l'ensemble correspond au besoin du client, l'application peut être déployée en production, mais, s'il y a des choses qui ne vont pas, il faut reprendre les développements en suivant le processus.

\par La mise en production se fait par le biais du responsable des systèmes informatiques d'A5Sys, Pierre \textsc{Vignot}. Il faut préparer un document de déploiement pour qu'il puisse installer le projet et ses dépendances sur des serveurs dédiés, plus sécurisés que les serveurs de la recette et aussi plus performants. Une fois l'application déployée en production, des retours des utilisateurs finaux et du client sont possible et peuvent impliquer une nouvelle phase de développement et de mise en production, en suivant les deux processus cités précédemment.

\subsection{Les rôles}

\par Notre méthodologie pseudo-scrum définit trois rôles : le propriétaire du produit (Client), le maître de mêlée (le chef de projet) et l'équipe de projet (développeurs).

\subsubsection{Client}

\par Pour ce projet c'est Hervé \textsc{Folleau}, responsable de la production du pôle APPS, qui est le client.

\subsubsection{Chef de projet}

\par Israëlle \textsc{Lagier}, conceptrice et développeuse en alternance endosse le rôle de cheffe de projet, elle est aidée par Hervé \textsc{Folleau} pour diriger le projet.

\subsubsection{Équipe de projet}

\par Cette équipe désigne les personnes qui vont réaliser les fonctionnalités voulus par le client, cette équipe constitue Angéline \textsc{Le Petit}, Israëlle \textsc{Lagier} (qui possède deux rôles) et moi-même.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "isae-report-template"
%%% End: 